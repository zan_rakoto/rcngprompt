module.exports = function (grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> (<%= pkg.homepage %>) */\n'
			},
			rcNgTooltip: {
				files: {
					'./js/rcNgPrompt.min.js': ['./js/rcNgPrompt.js']
				}
			}
		},
		jshint: {
			options: {
				ignores: ['./js/*.min.js','./*.min.js']
			},
			files: ['./js/*.js','./*.js']
		},
		cssmin: {
			options: {
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> (<%= pkg.homepage %>) */\n',
                ignores: ['./*.min.css']
			},
			minify:{
				files: {
					'css/rcNgPrompt.min.css': ['css/rcNgPrompt.css']
				}
			}
		},
        connect: {
            server: {
                options: {
                    port: 9002,
                    open: {
                        target: 'http://localhost:9002/example/index.html'
                    }
                }
            }
        },
        watch: {
            css: {
                files: ['css/*.css','!css/*.min.css'],
                tasks: ['cssmin'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['js/*.js','!js/*.min.js'],
                tasks: ['jshint'],
                options: {
                    livereload: true
                }
            },
            html: {
                files: ['example/*.html'],
                options: {
                    livereload: true
                }
            }
        }
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

	grunt.registerTask('default', ['jshint','connect','watch']);
	grunt.registerTask('build', ['jshint','uglify', 'cssmin']);
	grunt.registerTask('css', ['cssmin']);
    grunt.registerTask('js', ['jshint']);
};